# Google Gemini Matrix Bot

Chat with Gemini in Matrix!

Message the bot and it will forward all your messages to Gemini Pro. 
It will use matrix threads to respond to your message, so you can go back and forth.

## Installation
1. Obtain a [API key](makersuite.google.com/app/apikey) for Gemini
2. Copy ```config/config.json.example``` to ```config/config.json```
3. Change the required fields in the JSON file.
4. Iff you are in a non-supported country *and* use the ```docker compose``` approach, you can add a OpenVPN profile 
to ```config/vpn.ovpn``` and enable the ```NET_ADMIN``` capability in the ```docker-compose.yml``` file. 

### Docker Compose (recommended)
    
    cd docker
    sudo docker compose up
    sudo docker compose up -d # run in background

    sudo docker compose down # Stop

### Manually

    python3 -m venv venv
    source venv/bin/activate
    pip install -r src/requirements.txt
    cd src
    python3 bot.py
    

## Updating

### Docker Compose
    
    docker-compose pull
    docker-compose up --force-recreate --build -d

### Manually

    git pull
    # and restart

## Contributing
Contributions in the form of code, documentation, or suggestions are welcomed.
Feel free to contact me or open issues.

## TODO
- [ ] Add support for image upload and generation

## Licence
GNU GPLv3, see [License](LICENSE)
